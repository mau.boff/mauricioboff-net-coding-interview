﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class FlightsController : SecureFlightBaseController
{
    private readonly IService<Flight> _flightService;
    private readonly IRepository<Passenger> _passengerRepository;
    private readonly IRepository<PassengerFlight> _passengerFlightRepository;

    public FlightsController(IService<Flight> flightService, IMapper mapper, IRepository<PassengerFlight> passengerFlightRepository, IRepository<Passenger> passengerRepository)
        : base(mapper)
    {
        _flightService = flightService;
        _passengerFlightRepository = passengerFlightRepository;
        _passengerRepository = passengerRepository;
    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Get()
    {
        var flights = await _flightService.GetAllAsync();
        return GetResult<IReadOnlyList<Flight>, IReadOnlyList<FlightDataTransferObject>>(flights);
    }

    [HttpPut("{flightId}/{passengerId}")]
    [ProducesResponseType(typeof(FlightDataTransferObject), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public IActionResult Put(long flightId, string passengerId)
    {
        var result = _passengerFlightRepository.Add(new PassengerFlight
        {
            FlightId = flightId,
            PassengerId = passengerId
        });

        return Ok(result);
    }

    [HttpDelete("{flightId}/{passengerId}")]
    [ProducesResponseType(typeof(FlightDataTransferObject), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Delete(long flightId, string passengerId)
    {
        var result = _passengerFlightRepository.Remove(new PassengerFlight
        {
            FlightId = flightId,
            PassengerId = passengerId
        });

        var flights = await _passengerFlightRepository.FilterAsync(x => x.PassengerId == passengerId);
        if (!flights.Any())
        {
            var passenger = (await _passengerRepository.FilterAsync(x => x.Id == passengerId)).First();
            _passengerRepository.Remove(passenger);
        }

        return Ok(result);
    }

}